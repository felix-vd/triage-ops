# Reactive operations

[[_TOC_]]

This project runs a Ruby web service (a Rack application) which receives and react to webhooks from [the `gitlab-org` group](https://gitlab.com/gitlab-org) and [the `gitlab-com` group](https://gitlab.com/gitlab-com).

For every webhook request, the service passes the event payload to each processors sequentially.

The list of processors can be found under [`triage/processor/`](triage/processor/).

## Why don't we just use quick actions?

Quick actions are processed by GitLab itself, and permissions are enforced. For instance, someone that's not a member of a project can post `/approve`, but the quick action won't be interpreted, as the user doesn't have sufficient permissions.

*Reactive operations* are a way to circumvent some of those permissions for specific commands. For instance, we want to allow community contributors to set a group label ([CommandMrLabel](/triage/processor/community/command_mr_label.rb)), or request a review ([CommandMrRequestReview](/triage/processor/community/command_mr_request_review.rb)).

We also have some reactive commands that are *allowing actions that you cannot do with GitLab quick actions*, such as asking for help ([MergeRequestHelp](/triage/processor/community/command_mr_help.rb)), or giving feedback ([CodeReviewExperienceSlack](/triage/processor/community/command_mr_feedback.rb)).

## Specific topics

- [Architecture](./architecture.md)
- [Best Practices](./best_practices.md)
- [Run locally](./run_locally.md)
- [Implementing a new processor](./processor.md)
- [Production](./production.md)
- [Dashboard](./dashboard.md)
- [AppSec approval process](./appsec_approval_process.md)
