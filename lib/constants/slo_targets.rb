# frozen_string_literal: true

module SloTargets
  DEFAULT_SEVERITY_SLO_TARGETS = {
    "severity::1" => 30,
    "severity::2" => 60,
    "severity::3" => 90,
    "severity::4" => 120
  }.freeze

  VULNERABILITY_SEVERITY_SLO_TARGETS = {
    "severity::1" => 30,
    "severity::2" => 30,
    "severity::3" => 90,
    "severity::4" => 180
  }.freeze
end
