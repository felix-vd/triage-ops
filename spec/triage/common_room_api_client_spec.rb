# frozen_string_literal: true

require 'httparty'
require 'spec_helper'

require_relative '../../triage/triage/common_room_api_client'

RSpec.describe CommonRoomApiClient do
  let(:body) { { activityType: 'activityType', id: 11 } }
  let(:token) { 'crtoken' }
  let(:headers) do
    {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{token}"
    }
  end

  before do
    stub_env('COMMON_ROOM_API_TOKEN', token)
  end

  describe '.add_activity' do
    it 'makes a http post to the expected endpoint with the expected headers and payload' do
      expect(described_class).to receive(:post).with(
        '/source/22150/activity',
        headers: headers,
        body: body.to_json
      ).and_return({ 'status' => 'ok' })

      described_class.new.add_activity(body)
    end

    context 'when the API call returns a failure' do
      it 'throws an error' do
        expect(described_class).to receive(:post).with(
          '/source/22150/activity',
          headers: headers,
          body: body.to_json
        ).and_return({ status: 'failed' })

        expect { described_class.new.add_activity(body) }.to raise_error(StandardError)
      end
    end
  end
end
