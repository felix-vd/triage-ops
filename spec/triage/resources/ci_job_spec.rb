# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/resources/ci_job'

RSpec.describe Triage::CiJob do
  let(:project_id) { 1 }
  let(:job_id) { 2 }
  let(:job_name) { 'job name' }
  let(:web_url) { "https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672" }

  let(:failure_root_cause) { :default }
  let(:failure_type) { nil }
  let(:failure_trace) { nil }
  let(:failure_label) { nil }
  let(:job_trace_analyzer) do
    instance_double(Triage::PipelineFailure::JobTraceAnalyzer,
      failure_root_cause: failure_root_cause,
      failure_type: failure_type,
      failure_trace: failure_trace,
      failure_label: failure_label
    )
  end

  let(:rspec_results_json) { read_fixture("/reactive/job_artifacts/rspec.json") }
  let(:failed_tests_related_issues_json) { read_fixture("/reactive/job_artifacts/rspec-failed-test-issues.json") }

  include_context 'with stubbed GitLab artifact' do
    let(:artifacts) do
      {
        "rspec/rspec-retry-#{job_id}.json" => rspec_results_json,
        "rspec/#{job_id}-failed-test-issues.json" => failed_tests_related_issues_json
      }
    end
  end

  subject(:ci_job) { described_class.new(instance: :com, project_id: project_id, job_id: job_id, name: job_name, web_url: web_url) }

  before do
    job_trace = 'GitLab Job Trace'
    allow(Triage.api_client).to receive(:job_trace).with(project_id, job_id).and_return(job_trace)
    allow(Triage::PipelineFailure::JobTraceAnalyzer).to receive(:new).with(job_trace: job_trace).and_return(job_trace_analyzer)
  end

  describe "#failure_root_cause_label" do
    Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.each do |root_cause, label|
      context "with root cause being #{root_cause}" do
        let(:failure_root_cause) { root_cause }

        it 'returns "master-broken::undetermined"' do
          expect(ci_job.failure_root_cause_label).to eq(label)
        end
      end
    end
  end

  describe '#potential_responsible_group_labels' do
    context 'when failure_type is jest' do
      let(:failure_type) { :jest }

      it 'returns "frontend"' do
        expect(ci_job.potential_responsible_group_labels).to eq(['frontend'])
      end
    end

    context 'when failure_type is rspec' do
      let(:failure_type) { :rspec }

      it 'returns the group label detected from the RSpec metadata' do
        expect(ci_job.potential_responsible_group_labels).to eq(['missing product_group_label', 'group::group1', 'group::group1', 'group::group1', 'group::group2'])
      end

      context 'when RSpec results artifact is not a vald JSON' do
        let(:rspec_results_json) { "broken JSON!" }

        it 'returns []' do
          expect(ci_job.potential_responsible_group_labels).to eq([])
        end
      end
    end

    context 'when failure_type is workhorse' do
      let(:failure_type) { :workhorse }

      it 'returns the "source_code" label' do
        expect(ci_job.potential_responsible_group_labels).to eq(['group::source code'])
      end
    end

    context 'when failure_type is failed_to_pull_image' do
      let(:failure_root_cause) { :failed_to_pull_image }

      it 'returns the "container_registry" group label' do
        expect(ci_job.potential_responsible_group_labels).to eq(['group::container registry'])
      end
    end

    context 'when failure_type is gitlab_com_overloaded' do
      let(:failure_root_cause) { :gitlab_com_overloaded }

      it 'returns the "gitaly_cluster" group label' do
        expect(ci_job.potential_responsible_group_labels).to eq(['group::gitaly::cluster'])
      end
    end

    context 'when failure_type is runner_disk_full' do
      let(:failure_root_cause) { :runner_disk_full }

      it 'returns []' do
        expect(ci_job.potential_responsible_group_labels).to be_empty
      end
    end

    context 'when failure_type is job_timeout' do
      let(:failure_root_cause) { :job_timeout }

      it 'returns []' do
        expect(ci_job.potential_responsible_group_labels).to be_empty
      end
    end

    context 'when failure_type is infrastructure' do
      let(:failure_root_cause) { :infrastructure }

      it 'returns the infrastructure label' do
        expect(ci_job.potential_responsible_group_labels).to eq(['infrastructure'])
      end
    end
  end

  describe '#markdown_link' do
    it 'returns a Markdown link' do
      expect(ci_job.markdown_link).to eq("[#{job_name}](#{web_url})")
    end
  end

  describe "#test_failure_summary_markdown" do
    context 'with frontend trace' do
      let(:failure_type) { :jest }
      let(:failure_trace) { 'jest trace' }
      let(:failure_label) { '~frontend' }

      it 'returns a summary with the failure trace, a /label and /relate quick actions' do
        expect(ci_job.test_failure_summary_markdown).to eq(
          <<~MARKDOWN.chomp
          - [job name](https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672):

          jest trace
          /label ~frontend
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/438087
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/442468
          MARKDOWN
        )
      end
    end

    context 'with backend trace' do
      let(:failure_type) { :rspec }
      let(:failure_trace) { 'rspec trace' }
      let(:failure_label) { '~backend' }

      it 'does not contain any test failure stacktrace from after_script' do
        expect(ci_job.test_failure_summary_markdown).not_to include('stubbed after_script trace')
      end

      it 'returns a summary with the failure trace, a /label and /relate quick actions' do
        expect(ci_job.test_failure_summary_markdown).to eq(
          <<~MARKDOWN.chomp
          - [job name](https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672):

          rspec trace
          /label ~backend
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/438087
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/442468
          MARKDOWN
        )
      end
    end

    context 'with workhorse trace' do
      let(:failure_type) { :workhorse }
      let(:failure_trace) { 'workhorse trace' }
      let(:failure_label) { '~workhorse' }

      it 'returns a summary with the failure trace, a /label and /relate quick actions' do
        expect(ci_job.test_failure_summary_markdown).to eq(
          <<~MARKDOWN.chomp
          - [job name](https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672):

          workhorse trace
          /label ~workhorse
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/438087
          /relate https://gitlab.com/gitlab-org/gitlab/-/issues/442468
          MARKDOWN
        )
      end
    end

    context 'when related failure issues artifact is not a vald JSON' do
      let(:failure_type) { :workhorse }
      let(:failure_trace) { 'workhorse trace' }
      let(:failure_label) { '~workhorse' }
      let(:failed_tests_related_issues_json) { "broken JSON!" }

      it 'returns a summary with the failure trace, a /label quick action' do
        expect(ci_job.test_failure_summary_markdown).to eq(
          <<~MARKDOWN
          - [job name](https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672):

          workhorse trace
          /label ~workhorse
          MARKDOWN
        )
      end
    end
  end

  describe '#attribution_message_markdown' do
    let(:failure_type) { :rspec }

    context 'with RSpec test' do
      context 'with valid feature_categories' do
        it 'builds the correct attribution message' do
          expect(ci_job.attribution_message_markdown).to eq(
            <<~MARKDOWN.chomp
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/user_adds_lists_to_board_spec.rb:10
            - ~"group::group1" ~"Category:2" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
            - ~"group::group1" ~"Category:1" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]
            - ~"group::group1" ~"Category:1" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:5]
            - ~"group::group2" ~"Category:3" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:6]
            MARKDOWN
          )
        end
      end

      context 'with missing feature category' do
        let(:rspec_results_json) { read_fixture("/reactive/job_artifacts/rspec.json").gsub(/"feature_category": .+,$/, '"feature_category": null,') }

        it 'builds the correct attribution message' do
          expect(ci_job.attribution_message_markdown).to eq(
            <<~MARKDOWN.chomp
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/user_adds_lists_to_board_spec.rb:10
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:5]
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:6]
            MARKDOWN
          )
        end
      end

      context 'when RSpec results artifact is not a vald JSON' do
        let(:rspec_results_json) { "broken JSON!" }

        it 'returns ""' do
          expect(ci_job.attribution_message_markdown).to eq('')
        end
      end
    end

    context 'with workhorse test' do
      let(:failure_type) { :workhorse }
      let(:failed_rspec_test_metadata) { [] }

      it 'builds the correct attribution message and labels group::source code' do
        allow(Hierarchy::Group).to receive(:new).and_return(instance_double(Hierarchy::Group, label: ''))
        expect(Hierarchy::Group).to receive(:new).with('source_code').and_return(instance_double(Hierarchy::Group, label: 'group::source code'))
        expect(ci_job.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"group::source code" job name
          MARKDOWN
        )
      end
    end
  end

  describe '#rspec_run_time_summary_markdown' do
    include_context 'with stubbed GitLab artifact' do
      let(:artifacts) do
        {
          "knapsack/node_specs_expected_duration.json" => { 'spec1.rb' => 10.1, 'spec2.rb' => 5.1 }.to_json,
          "knapsack/job_name_#{project_id}_report.json" => { 'spec1.rb' => 20.1, 'spec2.rb' => 3.1 }.to_json
        }
      end
    end

    subject(:ci_job) { described_class.new(instance: :com, project_id: project_id, job_id: job_id, name: job_name, web_url: web_url) }

    it 'adds duration analysis' do
      expect(ci_job.rspec_run_time_summary_markdown).to eq(
        <<~MARKDOWN.chomp
          - [job name](https://gitlab.com/gitlab-org/gitlab/-/jobs/6225153672):

          <details><summary>Click to expand</summary>

          |file path|expected duration(s)|actual duration(s)|diff %|
          |---------|--------------------|------------------|------|
          |spec1.rb|10.1 seconds|20.1 seconds|+99%|
          |spec2.rb|5.1 seconds|3.1 seconds|-39%|

          </details>
        MARKDOWN
      )
    end
  end

  describe '#retry' do
    it 'returns a Markdown retry' do
      expect(Triage.api_client).to receive(:job_retry).with(project_id, job_id).and_return(Gitlab::ObjectifiedHash.new({}))

      expect(ci_job.retry).to be_a(Gitlab::ObjectifiedHash)
    end
  end
end
