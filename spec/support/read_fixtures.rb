# frozen_string_literal: true

module ReadFixtures
  extend self

  # Read a fixture with a given path
  # @return [String] the contents of the fixture file
  def read_fixture(*paths)
    File.read(File.expand_path(File.join("../fixtures", *paths), __dir__))
  end
end
