# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ci_components_label_processor'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CiComponentsLabelProcessor do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        from_gitlab_components?: from_gitlab_components
      }
    end

    let(:from_gitlab_org) { true }
    let(:from_gitlab_components) { false }
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "",
          "new_path" => "templates/gitlab.yaml"
        }
      ]
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open", "merge_request.reopen"]

  describe '#applicable?' do
    context 'when all conditions are met' do
      include_examples 'event is applicable'
    end

    context 'when MR is from components group with any MR change' do
      let(:from_gitlab_org) { false }
      let(:from_gitlab_components) { true }

      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "",
              "new_path" => "anything"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when MR is not from gitlab-org/gitlab or from components group' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when MR modifies file in templates/' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "templates/gitlab.yml",
              "new_path" => "templates/gitlab.yml"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when MR removes templates/' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "templates/gitlab.yml",
              "new_path" => "ee/gitlab.yml"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when MR change does not contain templates/' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/api/templates.yaml",
              "new_path" => "doc/api/templates/dockerfiles.md?"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when MR already contains ci::components' do
      let(:label_names) { ['ci::components'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'uses noteable_path to post a comment to add a customer label' do
      expect_comment_request(event: event, body: '/label ~ci::components') do
        subject.process
      end
    end
  end
end
