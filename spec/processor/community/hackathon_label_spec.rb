# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/hackathon_label'

RSpec.describe Triage::HackathonLabel do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        created_at: described_class::HACKATHON_START_DATE,
        object_kind: 'merge_request'
      }
    end

    let(:label_names) { ['Community contribution'] }
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when ~Hackathon label is present' do
      let(:label_names) { super() + [Labels::HACKATHON_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when merge request is created before hackathon' do
      before do
        allow(event).to receive(:created_at).and_return(described_class::HACKATHON_START_DATE - 1)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is created after hackathon' do
      before do
        allow(event).to receive(:created_at).and_return(described_class::HACKATHON_END_DATE + 1)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to label Hackathon' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          #{comment_mark}
          This merge request will be counted as part of the [running Hackathon](#{described_class::HACKATHON_TRACKING_ISSUE})!

          Check out the [Hackathon page](#{described_class::HACKATHON_WEBPAGE}) for more information! :tada:

          /label ~"Hackathon"
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
