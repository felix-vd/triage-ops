# frozen_string_literal: true

require_relative '../triage'

module Triage
  class PipelineJobsStatusManager
    FAILED_STATUS = 'failed'
    SUCCESS_STATUS = 'success'

    attr_reader :api_client, :project_id, :file_name, :branch_name

    def initialize(api_client:, project_id:, file_name:, branch_name:)
      @api_client = api_client
      @project_id = project_id
      @file_name = file_name
      @branch_name = branch_name
    end

    JobStatus = Struct.new(:name, :stage, :status, :allow_failure, :last_finished_at, :last_failed, :last_succeeded, keyword_init: true) do
      def self.from_job_status_hash(hash)
        new(
          name: hash.fetch('name'),
          stage: hash.fetch('stage'),
          status: hash.fetch('status'),
          allow_failure: hash.fetch('allow_failure'),
          last_finished_at: hash.fetch('last_finished_at'),
          last_failed: parse_history('last_failed', hash),
          last_succeeded: parse_history('last_succeeded', hash)
        )
      end

      def self.from_job_payload(job_payload, previous_job_status = nil)
        new(
          name: job_payload.name,
          stage: job_payload.stage,
          status: job_payload.status,
          allow_failure: job_payload.allow_failure,
          last_finished_at: job_payload.finished_at,
          last_failed: update_status_history(:last_failed, FAILED_STATUS, job_payload, previous_job_status),
          last_succeeded: update_status_history(:last_succeeded, SUCCESS_STATUS, job_payload, previous_job_status)
        )
      end

      def self.parse_history(key, hash)
        return unless hash.key?(key)

        Struct.new(:web_url, :finished_at).new(
          hash.fetch(key).fetch('web_url'),
          hash.fetch(key).fetch('finished_at')
        )
      end

      def self.update_status_history(status_history_key, status, job_payload, previous_job_status)
        if outdated_job?(job_payload, previous_job_status) || job_payload.status != status
          previous_job_status&.public_send(status_history_key)
        else
          Struct.new(:web_url, :finished_at).new(
            job_payload.web_url,
            job_payload.finished_at
          )
        end
      end

      def self.outdated_job?(job_payload, previous_job_status)
        if previous_job_status.nil?
          false
        elsif job_payload.finished_at.nil?
          true
        else
          previous_job_status.last_finished_at &&
            Time.parse(job_payload.finished_at) <= Time.parse(previous_job_status.last_finished_at)
        end
      end

      def to_hash
        history = {}

        if last_failed
          history[:last_failed] = {
            web_url: last_failed.web_url,
            finished_at: last_failed.finished_at
          }
        end

        if last_succeeded
          history[:last_succeeded] = {
            web_url: last_succeeded.web_url,
            finished_at: last_succeeded.finished_at
          }
        end

        {
          name: name,
          stage: stage,
          status: status,
          allow_failure: allow_failure,
          last_finished_at: last_finished_at
        }.merge(history)
      end

      def update(job_payload)
        previous_job_status = dup
        applicable_status = [FAILED_STATUS, SUCCESS_STATUS].include?(job_payload.status)

        return if self.class.outdated_job?(job_payload, previous_job_status) || !applicable_status

        self.status = job_payload.status
        self.allow_failure = job_payload.allow_failure
        self.last_finished_at = job_payload.finished_at

        self.last_failed = self.class.update_status_history(
          :last_failed,
          FAILED_STATUS,
          job_payload,
          previous_job_status
        )
        self.last_succeeded = self.class.update_status_history(
          :last_succeeded,
          SUCCESS_STATUS,
          job_payload,
          previous_job_status
        )
      end
    end

    def fetch_statuses_hash
      status_file_content == :file_not_found ? [] : JSON.parse(status_file_content)
    end

    def update_and_publish(jobs_payload:, commit_message:)
      previous_statuses_copy = statuses.map(&:dup)
      update_statuses(jobs_payload)
      publish_statuses(commit_message) if need_to_publish?(previous_statuses_copy)
    end

    private

    def status_file_content
      @status_file_content ||= begin
        api_client.file_contents(
          project_id,
          file_name,
          branch_name)
      rescue Gitlab::Error::NotFound
        :file_not_found
      end
    end

    def statuses
      @statuses ||= fetch_statuses_hash.map do |status_hash|
        JobStatus.from_job_status_hash(status_hash)
      end
    end

    def need_to_publish?(previous_statuses)
      previous_statuses.map(&:to_hash) != statuses.map(&:to_hash)
    end

    def update_statuses(jobs_payload)
      jobs_payload.each do |job_payload|
        previous_status = statuses.find do |status|
          status.name == job_payload.name && status.stage == job_payload.stage
        end

        if previous_status
          previous_status.update(job_payload)
        else
          new_status = JobStatus.from_job_payload(job_payload)
          statuses.push(new_status)
        end
      end
    end

    def publish_statuses(commit_message)
      api_client.public_send(
        status_file_content == :file_not_found ? :create_file : :edit_file,
        project_id,
        file_name,
        branch_name,
        JSON.pretty_generate(statuses.map(&:to_hash)),
        commit_message
      )
    end
  end
end
